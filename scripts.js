//Role keys/values for classic Town of Salem
var roles = 
{
    "Town Investigative" : ["Investigator", "Lookout", "Sheriff", "Spy"],
    "Town Protective" : ["Bodyguard", "Doctor"],
    "Town Killing" : ["Veteran", "Vigilante"],
    "Town Support" : ["Escort", "Mayor", "Medium", "Retributionist", "Transporter"],
    "Town Random" : ["Bodyguard", "Doctor", "Escort", "Investigator", "Lookout", "Mayor", "Medium", "Retributionist", "Sheriff", "Spy", "Transporter", "Veteran", "Vigilante"],
    "Mafia Random" : ["Blackmailer", "Consigliere", "Consort", "Disguiser", "Forger", "Framer", "Janitor"],
    "Neutral Evil" : ["Executioner", "Jester", "Witch"],
    "Neutral Killing" : ["Arsonist", "Serial Killer", "Werewolf"]
};

var alignment =
{
    "Town" : ["Town Protective", "Town Support", "Town Killing", "Town Investigative", "Town Random", "Bodyguard", "Doctor", "Escort", "Investigator", "Lookout", "Mayor", "Medium", "Retributionist", "Sheriff", "Spy", "Transporter", "Veteran", "Vigilante", "Jailor"],
    "Mafia" : ["Mafia Deception", "Mafia Killing", "Mafia Support", "Mafia Random", "Blackmailer", "Consigliere", "Consort", "Disguiser", "Forger", "Framer", "Janitor", "Godfather", "Mafioso"],
    "Neutral Evil" : ["Neutral Evil", "Executioner", "Jester", "Witch"],
    "Neutral Killing" : ["Neutral Killing", "Serial Killer", "Werewolf", "Arsonist"]
};

//Role lists for game modes
var roleList = 
[
    [],
    //Ranked / RP
    ["Jailor", "Town Investigative", "Town Investigative", "Town Protective", "Town Killing", "Town Support", "Town Random", "Town Random", "Town Random",
    "Godfather", "Mafioso", "Mafia Random", "Mafia Random", "Neutral Evil", "Neutral Killing"],
    []
];

var unclaimedRoles = [];

var usedRoleList = [];

var claimedRoles = {};

function buildTable(id)
{
    //initial table header
    var playerTable = "<table class=\"table table-bordered table-dark\"><thead><tr><th scope=\"col\" class=\"w-100\">#<\/th><th scope=\"col\">Claims<\/th><th scope=\"col\" class=\"w-50\">Confirmed<\/th><th scope=\"col\" class=\"w-50\">Dead<\/th><\/tr><\/thead><tbody>"
    
    //initial rolelist for dropdown
    var dropdownRoles = new Array();
    for(i=0;i<roleList[id].length;i++)
    {
        //if role, and not in list, add to list
        if(!(roleList[id][i] in roles) && !dropdownRoles.includes(roleList[id][i]))
            dropdownRoles.push(roleList[id][i]);
        //if category, add all roles from category to list that arent in list
        else if(roleList[id][i] in roles)
        {
            roles[roleList[id][i]].forEach(element => {
                if(!dropdownRoles.includes(element))
                    dropdownRoles.push(element);
            });
        }
    }
    //generate javascript
    var dropdownstr = "<option selected value=\"None\">None</option>";
    dropdownRoles.forEach(element => {
        dropdownstr += "<option value=\"" + element +"\">" + element + "</option>"
    });
    
    //amount of players is equal to role list length
    for(i=0;i<roleList[id].length;i++)
    {
        //id
        playerTable += "<tr><th scope=\"row\">" + (i+1) + "<\/th>";
        //claim dropdown
        playerTable += "<td><select class=\"custom-select\" id=\"role_" + i + "\" onchange=\"onRoleChange(this)\">" + dropdownstr + "</select><\/td>";
        //Confirmed button
        playerTable += "<td><button type=\"button\" class=\"btn btn-success\">Y</button><\/td>";
        //Dead button
        playerTable += "<td><button type=\"button\" class=\"btn btn-danger\">X</button><\/td><\/tr>";
        $('#tableContainer').html(playerTable);
    }

    //initial table footer
    playerTable += "</tbody></table>"
    
    unclaimedRoles = roleList[id];
    usedRoleList = roleList[id];
    updateUnclaimedRoles();
}

function updateUnclaimedRoles()
{
    var str = "";
    unclaimedRoles.forEach(element => 
    {
        str += "<span class=\"badge badge-" + colorFromAlignment(element) + " " + element.replace(" ", "_") + "\">" + element + "</span> "
    })
    $('#unclaimedContainer').html(str);
}

function colorFromAlignment(rolename)
{
    if(alignment["Town"].includes(rolename)) return "success";
    else if(alignment["Mafia"].includes(rolename)) return "danger";
    else if(alignment["Neutral Evil"].includes(rolename)) return "warning";
    else if(alignment["Neutral Killing"].includes(rolename)) return "primary";
    else return "secondary";
}

function onRoleChange(selectBox)
{
    var id = selectBox.id;
    if(claimedRoles[id])
    {
        //add back role
        unclaimedRoles.push(claimedRoles[id]);
    }
    //if selected value is none, set claimedRole to null
    if(selectBox.value == "None") claimedRoles[id] == null;
    //findFirstMatch should always find a match if it is a legal role
    else
    {
        var closestRole = findFirstMatch(selectBox.value, unclaimedRoles);
        if(closestRole == null) alert("Illegal role!");
        else
        {
            //remove from unclaimedRoles and add to claimedRole
            unclaimedRoles.splice(unclaimedRoles.indexOf(closestRole), 1);
            claimedRoles[id] = closestRole;
        }
    }

    updateUnclaimedRoles();
}

function findFirstMatch(roleName, list)
{
    //check for a direct match in unclaimedRoles
    if(list.includes(roleName)) return roleName;
    //iterate through roles
    for(let key in roles)
    {
        if(roles[key].includes(roleName) && list.includes(key)) return key;
    }
    //return null if nothing found
    return null;
}